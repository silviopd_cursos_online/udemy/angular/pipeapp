import {LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SharedModule} from "./shared/shared.module";
import {VentasModule} from "./ventas/ventas.module";

//cambiar local de la app
import {registerLocaleData} from '@angular/common';

import localEsPE from '@angular/common/locales/es-PE';
import localFr from '@angular/common/locales/fr';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

registerLocaleData(localEsPE);
registerLocaleData(localFr);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    VentasModule
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'es-PE'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
