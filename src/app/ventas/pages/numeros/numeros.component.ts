import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-numeros',
  templateUrl: './numeros.component.html',
  styleUrls: ['./numeros.component.css']
})
export class NumerosComponent implements OnInit {

  ventasNetas: number = 3423423424123.322944
  porcentaje: number = 0.4839912

  constructor() {
  }

  ngOnInit(): void {
  }

}
