import {Component, OnInit} from '@angular/core';
import {interval} from "rxjs";

@Component({
  selector: 'app-no-comunes',
  templateUrl: './no-comunes.component.html',
  styleUrls: ['./no-comunes.component.css']
})
export class NoComunesComponent implements OnInit {

  // i18nSelect
  nombre: string = 'Juana'
  genero: string = 'femenino'

  invitacionMapa = {
    'femenino': 'invitarla',
    'masculino': 'invitarlo'
  }

  // i18nPlural
  clientes: string[] = ['Juan', 'Pedro', 'María', 'Marta', 'Juana', 'José', 'Dario', 'Christian', 'Ricardo']
  clientesMapa = {
    '=0': 'no tenemos ningun cliente esperando',
    '=1': 'tenemos un cliente esperando',
    'other': 'tenemos # clientes esperando'
  }

  // KeyValuePipe
  persona = {
    nombre: 'Juan',
    apellido: 'Pérez',
    edad: 23,
    email: 'silviopd01@gmail.com'
  }

  //JsonPipe
  heroes = [
    {
      nombre: 'Superman',
      poder: 'Super Fuerza'
    },
    {
      nombre: 'Batman',
      poder: 'Inteligencia'
    },
    {
      nombre: 'Flash',
      poder: 'Velocidad'
    },
    {
      nombre: 'Wonder',
      poder: 'Invisibilidad'
    }
  ]

  //AsyncPipe
  miObservable = interval(1000)

  valorPromesa = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('Tenemos data de la promesa')
    }, 3500)
  })

  constructor() {
  }

  ngOnInit(): void {
  }

  cambiarPersona = () => {
    this.nombre = 'Silviopd'
    this.genero = 'masculino'
  }

  borrarCliente = () => {
    this.clientes.pop()
  }

  reset = (value: number) => {
    if (value == 1) {
      this.nombre = 'Juana'
      this.genero = 'femenino'
    }
    if (value == 2) {
      this.clientes = ['Juan', 'Pedro', 'María', 'Marta']
    }
  }
}
