import {Component, OnInit} from '@angular/core';
import {Color, Heroe} from "../../interfaces/ventas.interfaces";

@Component({
  selector: 'app-ordenar',
  templateUrl: './ordenar.component.html',
  styleUrls: ['./ordenar.component.css']
})
export class OrdenarComponent implements OnInit {

  enMayusculas: boolean = true;
  ordenarPor: string = ''

  heroes: Heroe[] = [
    {
      name: "Flash",
      fly: true,
      color: Color.verde,
    }, {
      name: "Aquaman",
      fly: true,
      color: Color.azul,
    },
    {
      name: "Wonder",
      fly: true,
      color: Color.azul
    },
    {
      name: "Batman",
      fly: false,
      color: Color.rojo,
    },

    {
      name: "Superman",
      fly: false,
      color: Color.rojo,
    }
  ]

  constructor() {
  }

  ngOnInit(): void {
  }

  changeMayusculas = () => {
    this.enMayusculas = !this.enMayusculas;
  }

  ordenarPorValores = (value: string) => {
    this.ordenarPor = value;
  }
}
