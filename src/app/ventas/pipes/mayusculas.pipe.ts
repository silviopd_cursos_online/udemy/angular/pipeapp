import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'mayusculas'
})
export class MayusculasPipe implements PipeTransform {

  // transform(value: unknown, ...args: unknown[]): unknown {
  //   return null;
  // }

  transform(value: string, arg: boolean = true): string {
    if (arg) {
      return value.toUpperCase();
    } else {
      return value.toLowerCase();
    }
  }
}
