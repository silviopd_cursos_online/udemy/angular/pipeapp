import {Pipe, PipeTransform} from '@angular/core';
import {Color, Heroe} from "../interfaces/ventas.interfaces";

@Pipe({
  name: 'ordenar'
})
export class OrdenarPipe implements PipeTransform {

  transform(value: Heroe[], ordenarPor: string = ''): Heroe[] {
    switch (ordenarPor) {
      case 'nombre':
        return value.sort((a, b) =>
          (a.name > b.name) ? 1 : -1
        );
      case 'vuela':
        return value.sort((a, b) =>
          (a.fly > b.fly) ? -1 : 1
        );
      case 'color':
        return value.sort((a, b) =>
          (Color[a.color] > Color[b.color]) ? 1 : -1
        );
      default:
        return value;
    }

  }

}
